
all: beegsnek smolsnek

beegsnek: smolsnek.c
	gcc -s -Os -o beegsnek smolsnek.c

# I'm just gonna throw all these at it and hope it does something.
smolsnek: smolsnek.c
	gcc -s -Os \
		-o smolsnek \
		-fno-stack-protector \
		-fomit-frame-pointer \
		-ffunction-sections -fdata-sections -Wl,--gc-sections \
		-falign-functions=1 \
		-falign-jumps=1 \
		-falign-loops=1 \
		-fno-unwind-tables \
		-fno-asynchronous-unwind-tables \
		-fno-math-errno \
		-fno-unroll-loops \
		-fmerge-all-constants \
		-fno-ident \
		-Wl,-z,norelro \
		-Wl,--hash-style=gnu \
		-Wl,--build-id=none \
		smolsnek.c
	strip smolsnek
	#upx smolsnek

clean:
	rm -f beegsnek smolsnek *~

.PHONY: all
