#include <stdlib.h>
#include <sys/time.h>
#include <unistd.h>
#include <poll.h>
#include <string.h>

#include <termios.h>

#define GP_WIDTH 15
#define GP_HEIGHT 10
#define GP_START_X 5
#define GP_START_Y 2
#define GP_START_DIR 0
#define GP_START_SCORE 2
#define GP_TIMESTEP_MS 500

#define DIR_EAST 0
#define DIR_NORTH 1
#define DIR_WEST 2
#define DIR_SOUTH 3

typedef struct {
    int x, y;
} v2;

static int snek_itoa(char* buf, int v) {
    if (v == 0) {
        buf[0] = '0';
        return 1;
    }

    // Temp buffer before we reverse the numbers.
    char tmp[16];
    int n = 0;

    // Put the digits into the buffer in reverse order.
    while (v != 0) {
        int digit = v % 10;
        v /= 10;
        tmp[n++] = '0' + digit;
    }

    // Put them into the dest buf in the correct order.
    for (int i = 0; i < n; i++) {
        buf[i] = tmp[n - i - 1]; // ez
    }

    return n;
}

// Moves the cursor in *screen space*, not world space.
static void cursor_mov(int x, int y) {
    char buf[64];
    strcpy(buf, "\x1b[");
    int n = 2;
    n += snek_itoa(buf + n, y);
    buf[n++] = ';';
    n += snek_itoa(buf + n, x);
    buf[n++] = 'H';
    write(1, buf, n);
}

static void draw_cell_char(char c, int x, int y) {
    // + 2 because this is in screen space and we have a border
    cursor_mov(x + 2, y + 2);
    write(1, &c, 1);
    cursor_mov(1, 1);
}

static void draw_frame() {
    // Draw screen and reposition cursor.
    write(1, "\x1b[2J\x1b[1;1H", 10);

    // Draw top bar.
    write(1, "O", 1);
    for (int i = 0; i < GP_WIDTH; i++) {
        write(1, "-", 1);
    }
    write(1, "O", 1);

    // Draw horizontal bars.
    for (int i = 0; i < GP_HEIGHT; i++) {
        cursor_mov(1, i + 2);
        write(1, "|", 1);
        cursor_mov(GP_WIDTH + 2, i + 2);
        write(1, "|", 1);
    }

    // Draw bottom bar.
    cursor_mov(1, GP_HEIGHT + 2);
    write(1, "O", 1);
    for (int i = 0; i < GP_WIDTH; i++) {
        write(1, "-", 1);
    }
    write(1, "O", 1);

}

typedef struct {
    v2 food;
    v2* body;
    int score;
    int head_index;
    int dir;
} gamestate;

static v2 next_tile(v2 cur, int dir) {
    switch (dir) {
    case DIR_EAST:
        return (v2){cur.x + 1, cur.y};
    case DIR_NORTH:
        return (v2){ .x = cur.x, .y = cur.y - 1 };
    case DIR_WEST:
        return (v2){ .x = cur.x - 1, .y = cur.y };
    case DIR_SOUTH:
        return (v2){ .x = cur.x, .y = cur.y + 1 };
    }
    return (v2){0, 0};
}

#define STATE_OK 0
#define STATE_AUTOCANNIBALISM 1
#define STATE_OOB 2

static int do_game_step(gamestate* g) {

    v2 next_head = next_tile(g->body[g->head_index], g->dir);

    // Check for bounds.
    if (next_head.x < 0 || next_head.x >= GP_WIDTH || next_head.y < 0
        || next_head.y >= GP_HEIGHT) {
        return STATE_OOB;
    }

    // Check for self-intersection.
    for (int i = 0; i < g->score; i++) {
        if (g->body[i].x == next_head.x && g->body[i].y == next_head.y) {
            return STATE_AUTOCANNIBALISM;
        }
    }

    if (next_head.x == g->food.x && next_head.y == g->food.y) {

        v2* nbody = malloc(sizeof(v2) * (g->score + 1));

        // Copy over the cells.
        nbody[0] = next_head;
        for (int i = 0; i < g->score; i++) {
            nbody[i + 1] = g->body[(g->head_index + i + 1) % g->score];
        }

        // Cleanup
        free(g->body);
        g->body = nbody;
        g->head_index = 0;
        g->score++;
        draw_cell_char('#', next_head.x, next_head.y);

        // Pick and draw new food.
        g->food = (v2){ rand() % GP_WIDTH, rand() % GP_HEIGHT };
        draw_cell_char('%', g->food.x, g->food.y);

    } else {
        // Sneaky sneaky.
        v2 old_tail = g->body[(g->head_index + 1) % g->score];
        draw_cell_char(' ', old_tail.x, old_tail.y);
        g->body[(g->head_index + 1) % g->score] = next_head;
        g->head_index = (g->head_index + 1) % g->score;
        draw_cell_char('#', next_head.x, next_head.y);
    }

    return STATE_OK;

}

static void process_input(gamestate* g, char c) {
    switch (c) {
    case 'd':
    case 'D':
        {
            g->dir = DIR_EAST;
            break;
        }
    case 'w':
    case 'W':
        {
            g->dir = DIR_NORTH;
            break;
        }
    case 'a':
    case 'A':
        {
            g->dir = DIR_WEST;
            break;
        }
    case 's':
    case 'S':
        {
            g->dir = DIR_SOUTH;
            break;
        }
    default:
        // do nothing
        ;
    }
}

static void init_game(gamestate* g) {

    // TODO Properly seed.
    srand(1337);

    g->score = GP_START_SCORE;
    g->dir = GP_START_DIR;

    g->food = (v2){ rand() % GP_WIDTH, rand() % GP_HEIGHT };
    draw_cell_char('%', g->food.x, g->food.y);

    g->body = malloc(sizeof(v2) * GP_START_SCORE);
    for (int i = 0; i < GP_START_SCORE; i++) {
        v2 v = (v2){ GP_START_X - i, GP_START_Y };
        draw_cell_char('#', v.x, v.y);
        g->body[i] = v;
    }

}

static struct termios saved;
static struct termios tattr;

static void reset_terminal(void) {
    tcsetattr(STDIN_FILENO, TCSANOW, &saved);
}

static void config_terminal(void) {
    // Source: https://www.gnu.org/software/libc/manual/html_node/Noncanon-Example.html

    tcgetattr(STDIN_FILENO, &saved);
    atexit(reset_terminal);

    tcgetattr(STDIN_FILENO, &tattr);
    tattr.c_lflag &= ~(ICANON|ECHO); /* Clear ICANON and ECHO. */
    tattr.c_cc[VMIN] = 1;
    tattr.c_cc[VTIME] = 0;
    tcsetattr(STDIN_FILENO, TCSAFLUSH, &tattr);
}

int main(void) {

    int res = 0;

    gamestate gs;

    draw_frame();
    init_game(&gs);
    config_terminal();

    while (1) {

        struct timeval deadline;
        gettimeofday(&deadline, NULL);
        deadline.tv_usec += (GP_TIMESTEP_MS * 1000) % 1000000;
        deadline.tv_sec += GP_TIMESTEP_MS / 1000;

        // Keep trying to read input for GP_TIMESTEP_MS milliseconds.
        while (1) {

            struct timeval now;
            gettimeofday(&now, NULL);

            int wait_ms = (deadline.tv_sec - now.tv_sec) * 1000 + (deadline.tv_usec - now.tv_usec) / 1000;

            struct pollfd fds[1] = { (struct pollfd){0, POLLIN, 0 } };
            int pres = poll((struct pollfd*) &fds, 1, wait_ms);

            if (pres < 0) {
                exit(1);
            } else {
                if (pres > 0) {
                    char c = '\0';
                    int rres = read(0, &c, 1);
                    if (rres < 0) {
                        exit(2);
                    } else {
                        process_input(&gs, c);
                    }
                } else {
                    // We're done waiting.
                    break;
                }
            }

        }

        // Compute next game state.
        res = do_game_step(&gs);
        if (res != 0) {
            break;
        }
    }

    usleep(1000000);
    write(1, "\x1b[2J\x1b[1;1HScore: ", 17);
    char scorebuf[20];
    int scorelen = snek_itoa(scorebuf, gs.score);
    scorebuf[scorelen] = '\n';
    write(1, scorebuf, scorelen + 1);
    exit(0);

    return 0;
}
